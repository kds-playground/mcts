import players
from nim import str_to_action
from nim import Environment
import utils

if __name__ == "__main__":
    start_board = Environment([2, 3, 1], 0, 2)
    player = Environment().turn()

    player1 = players.MctsPlayer(exploration_param = 0.5)
    player2 = players.HumanPlayer(str_to_action)

    # here we are training the MctsPlayer by playing it against itself for multiple rounds 
    # and determining the results of these games; here the MctsPlayer is self-playing for 10000 rounds
    for _ in range(10000):
        log = utils.play(start_board, [player1, player1])
        (last_environment, _, _) = log[-1]
        game_value = [last_environment.value(k) for k in range(last_environment.num_agents())]
        player1.cache[last_environment].backpropagation(game_value)

    log = utils.play(start_board, [player1, player2])
    print("Game Over! The winner of the game is: Player", player)
    